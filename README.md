![](./img/cr_logo.png)


# How to get to the CR Classrooms on the Referenc Construction Site

- Start by going to this [Landmark](https://goo.gl/maps/QYPYJhCaQdfikHUa6):  
```
    Newton’s Apartments GmbH & Co  
    Kühlwetterstraße 8, 52072 Aachen
```
- From there, follow the light blue arrow in the image below, going upward towards the trains
- _Very important:_ Keep on going up until you are adjacent to the rail **(the red arrow)**
- At this point, you should be on a road _adjacent_ to the rail, the rail is on your _immediate left_, if you find a dead end or in a parking lot, then you did not walk all the way up
- Stay on that road, it will soon change from paved to unpaved, keep going!
- After 800 meters or so (while being adjacent to the rail), you will see a gate on the right side, with white/yellow/blue containers inside. This is the reference construction site. You have arrived! Enter the gate.
- Follow the signs to the classroom. This is the white container on the left hand when entering the site.
![reference_site.png](./img/reference_site.png)

# Time Needed to Get There

Please account for 15-20 minutes by foot from the Aachen West train station or the Reiff-Museum building, or for 5 minutes by bike (bike parking provided on the reference site)

# Safety

- In order to be able to participate in classes in the CR classrooms on the Reference Construction site, please make sure to:
- Not wear slippers, sandals, or any kind of open shoes. Otherwise you will be denied entry to the site!
- Work towards obtaining proper safety shoes
- Always use _the backside of the containers_ when entering the classroom or going to the toilet
![back_of_container.png](./img/back_of_container.png)
- For students joining from abroad, get into the habit of checking the weather daily (including installing a rain radar app on your phone) before coming to the site and bring jackets (it can get cold!), umbrellas, rain coats, etc. when needed. Remember, rain on the construction site means mud and hence you should always dress accordingly.

# Things to Bring

- One of the following (i.e. the 3G regulation): a proof of vaccination, a proof of recovery, or a test result.
- A medical face mask (to be worn at all time during the class)
- A water bottle if you do not wish to buy water from us
- Safety shoes
- A jacket if it is cold
